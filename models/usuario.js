const mongoose = require("mongoose");
const Reserva = require("./reserva");
const Schema = mongoose.Schema;
const bcrypt = require("bcrypt");
const saltRounds = 10;


const validateEmail = function (email) {  
  const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return re.test(email);
}


const usuarioSchema = new Schema({
  nombre: {
    type: String,
    trim: true,
    required:[true,'El nombre es obligatorio']
  },
  email:{
    type: String,
    trim:true,
    required:[true,'El email es obligatorio'],
    lowercase: true,
    validate:[validateEmail,"Ingresá un email valido."],
    match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/]
  },
  password:{
    type: String,
    required:[true,'la contraseña es obligatoria'],
  },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verified: {
        type: Boolean,
        default: false
    }
});


//userSchema.plugin(uniqueValidator, {message: 'This email {PATH} already exits'});

userSchema.pre('save', function (next) {
    if(this.isModified('password')){
        this.password = bcrypt.hashSync(this.password, saltRounds); //encriptamos el pass el saltronds es para encripte de manera diferente(aleatorio)
    }
    next();
});

userSchema.methods.validPassword = function (password) {  
    return bcrypt.compareSync(password, this.password);
};




usuarioSchema.methods.reservar = function (biciId, desde, hasta, cb) {
  const reserva = new Reserva({
    usuario: this._id,
    bicicleta: biciId,
    desde: desde,
    hasta: hasta,
  });
  console.log(reserva);
  reserva.save(cb);
};

module.exports = mongoose.model("Usuario", usuarioSchema);