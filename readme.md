## Proyecto Bicicletas Modulo 1
**Bienvenidos, este proyecto se basa en una web donde visualizar bicicletas en un mapa basándonos en node para el backend.**

**Antes de ejecutar el comando npm run devstart, asegurarse de instalar las liberias con npm i**



comandos utiles durante el trayecto: 
npm i --save express -g
npm install --save express-generator -g
el npm i nodemon --save-dev detecta que cambiamos un archivo y reinicia el sv automaticamente

 > agregar jasmine para testear el proyecto

            Primero, instalarlo globalmente para poder ejecutarlo directamente desde la terminal.
            1) npm install –g jasmine
            Luego, lo agregamos como dependencia desarrollo.
            2) npm install --save-dev jasmine
            Luego, debemos inicializar el módulo haciendo:
            3) node node_modules/Jasmine/bin/Jasmine init
            Y finalmente, agregamos una tarea en el package.json, en la sección de scripts:
            4) “scripts”: {“test”: jasmine}
            De esta forma, si ejecutamos npm test, se corren todos los tests que tengamos en
            nuestro proyecto. 
            5) para ejecutar podemos usar npm run test, o jasmine spec/models/bicicleta_test.spec.js 